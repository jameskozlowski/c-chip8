﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chip8Emu
{
    class Chip8
    {
        public byte[] VideoMemory { get; }
        public bool NeedsScreenRefresh { get; set; }
        public bool NeedsSoundPlayed { get; set; }
        public byte[] KeyState { get; set; }

        private byte[] Memory;            //4096 byte memory
        private ushort pc;                //16 bit memory pointer
        private ushort I;                 //16 bit I register
        private byte[] V;                 //8 bit V registers
        private ushort[] Stack;           //Stack register
        private byte StackPointer;        //Stack Pointer
        private byte[] Fontset;

        private long LastTick;
        private long LastTick2;
        private byte TimerDelay;
        private byte TimerSound;
        private Stopwatch sw = Stopwatch.StartNew();

        public Chip8()
        {
            Memory = new byte[4096];
            VideoMemory = new byte[64 * 32];
            V = new byte[16];
            KeyState = new byte[16];
            Stack = new ushort[16];


            Fontset = new byte[80]                                            //Default Fonts
            {
                0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
		        0x20, 0x60, 0x20, 0x20, 0x70, // 1
		        0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
		        0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
		        0x90, 0x90, 0xF0, 0x10, 0x10, // 4
		        0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
		        0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
		        0xF0, 0x10, 0x20, 0x40, 0x40, // 7
		        0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
		        0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
		        0xF0, 0x90, 0xF0, 0x90, 0x90, // A
		        0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
		        0xF0, 0x80, 0x80, 0x80, 0xF0, // C
		        0xE0, 0x90, 0x90, 0x90, 0xE0, // D
		        0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
		        0xF0, 0x80, 0xF0, 0x80, 0x80  // F
	        };
        }

        public void Reset()
        {
            pc = 0x200;                 // Program counter starts at 0x200
            StackPointer = 0;
            I = 0;
            TimerDelay = 0;
            TimerSound = 0;
            TimerSound = 0;
            NeedsSoundPlayed = false;
            for (int i = 0; i < 16; ++i)
                V[i] = 0;
            for (int i = 0; i < 16; ++i)
                KeyState[i] = 0;
            for (int i = 0; i < 64 * 32; ++i)
                VideoMemory[i] = 0;
            NeedsScreenRefresh = false;
            for (int i = 0; i < 80; ++i)
                Memory[i] = Fontset[i];
            LastTick = sw.ElapsedMilliseconds;
            LastTick2 = sw.ElapsedMilliseconds; 
        }

        public void LoadFile(string filename)
        {
            //   using (BinaryReader reader = new BinaryReader(File.Open(filename, FileMode.Open)))
            //  {
            //      while (reader.)
            //     reader.ReadByte
            //  }

            using (Stream source = File.OpenRead(filename))
            {
                source.Read(Memory, 0x200, (int)source.Length);
            }

        }

        public void EmulateCycle()
        {
            if (sw.ElapsedMilliseconds < LastTick + 1)
                return;
            LastTick = sw.ElapsedMilliseconds;

            ushort opcode = (ushort)(Memory[pc] << 8 | Memory[pc + 1]);

            #region opcodes
            switch (opcode & 0xF000)
            {
                #region 0x0000 opcodes
                case 0x0000:
                    switch (opcode & 0x000F)
                    {
                        case 0x0000: // 0x00E0: Clears the screen        
                            for (int i = 0; i < 64 * 32; ++i)
                                VideoMemory[i] = 0;
                            NeedsScreenRefresh = true;
                            pc += 2;
                            break;


                        case 0x000E: // 0x00EE: Returns from subroutine          
                            pc = Stack[StackPointer - 1];
                            StackPointer--;
                            pc += 2;
                            break;

                        default:
                            throw new Exception("Unknown opcode [0x0000]: " + opcode);
                    }
                    break;
                case 0x1000: // 1NNN: Jups to address
                    pc = (ushort)(opcode & 0x0FFF);

                    break;

                case 0x2000: // 2NNN 	Calls subroutine at NNN.
                    Stack[StackPointer] = pc;
                    ++StackPointer;
                    pc = (ushort)(opcode & 0x0FFF);
                    break;
                case 0x3000: // 3XNN 	Skips the next instruction if VX equals NN.
                    if (V[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF))
                        pc += 4;
                    else
                        pc += 2;
                    break;
                case 0x4000: // 4XNN 	Skips the next instruction if VX doesn't equal NN.
                    if (V[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF))
                        pc += 4;
                    else
                        pc += 2;
                    break;
                case 0x5000: // 5XY0 	Skips the next instruction if VX equals VY.
                    if (V[(opcode & 0x0F00) >> 8] == V[(opcode & 0x00F0) >> 4])
                        pc += 4;
                    else
                        pc += 2;
                    break;
                case 0x6000: // 6XNN 	Sets VX to NN.
                    V[(opcode & 0x0F00) >> 8] = (byte)((opcode & 0x00FF));
                    pc += 2;
                    break;
                case 0x7000: // 7XNN 	Adds NN to VX.
                    V[(opcode & 0x0F00) >> 8] += (byte)((opcode & 0x00FF));
                    pc += 2;
                    break;
                case 0x8000:
                    switch (opcode & 0x000F)
                    {
                        case 0x0000: // 8XY0 	Sets VX to the value of VY.
                            V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4];
                            pc += 2;
                            break;
                        case 0x0001: // 8XY1    Sets VX to VX or VY.
                            V[(opcode & 0x0F00) >> 8] = (byte)(V[(opcode & 0x0F00) >> 8] | V[(opcode & 0x00F0) >> 4]);
                            pc += 2;
                            break;
                        case 0x0002: // 8XY2    Sets VX to VX and VY.
                            V[(opcode & 0x0F00) >> 8] = (byte)(V[(opcode & 0x0F00) >> 8] & V[(opcode & 0x00F0) >> 4]);
                            pc += 2;
                            break;
                        case 0x0003: // 8XY1    Sets VX to VX xor VY.
                            V[(opcode & 0x0F00) >> 8] = (byte)(V[(opcode & 0x0F00) >> 8] ^ V[(opcode & 0x00F0) >> 4]);
                            pc += 2;
                            break;
                        case 0x0004: // 8XY4 	Adds VY to VX. VF is set to 1 when there's a carry, and to 0 when there isn't.       
                            if (V[(opcode & 0x00F0) >> 4] > (0xFF - V[(opcode & 0x0F00) >> 8]))
                                V[0xF] = 1; //carry
                            else
                                V[0xF] = 0;
                            V[(opcode & 0x0F00) >> 8] += V[(opcode & 0x00F0) >> 4];
                            pc += 2;
                            break;
                        case 0x0005: // 8XY5 	VY is subtracted from VX. VF is set to 0 when there's a borrow, and 1 when there isn't.  
                            if (V[(opcode & 0x00F0) >> 4] > V[(opcode & 0x0F00) >> 8])
                                V[0xF] = 0; // there is a borrow
                            else
                                V[0xF] = 1;
                            V[(opcode & 0x0F00) >> 8] -= V[(opcode & 0x00F0) >> 4];
                            pc += 2;
                            break;
                        case 0x0006: // 8XY6 	Shifts VX right by one. VF is set to the value of the least significant bit of VX before the shift.
                            V[0xF] = (byte)((V[(opcode & 0x0F00) >> 8]) & 0x1);
                            V[(opcode & 0x0F00) >> 8] >>= 1;
                            pc += 2;
                            break;
                        case 0x0007: // 8XY7 	Sets VX to VY minus VX. VF is set to 0 when there's a borrow, and 1 when there isn't.
                            if (V[(opcode & 0x0F00) >> 8] > V[(opcode & 0x00F0) >> 4])  // VY-VX
                                V[0xF] = 0; // there is a borrow
                            else
                                V[0xF] = 1;
                            V[(opcode & 0x0F00) >> 8] = (byte)(V[(opcode & 0x00F0) >> 4] - V[(opcode & 0x0F00) >> 8]);
                            pc += 2;
                            break;
                        case 0x000E: // 8XYE 	Shifts VX left by one. VF is set to the value of the most significant bit of VX before the shift.
                            V[0xF] = (byte)(V[(opcode & 0x0F00) >> 8] >> 7);
                            V[(opcode & 0x0F00) >> 8] <<= 1;
                            pc += 2;
                            break;
                        default:
                            throw new Exception("Unknown opcode [0x0000]: " + opcode);
                    }
                    break;
                #endregion

                case 0x9000: // 9XY0 	Skips the next instruction if VX doesn't equal VY.
                    if (V[(opcode & 0x0F00) >> 8] != V[(opcode & 0x00F0) >> 4])
                        pc += 4;
                    else
                        pc += 2;
                    break;

                case 0xA000: // ANNN: Sets I to the address NNN
                    I = (ushort)(opcode & 0x0FFF);
                    pc += 2;
                    break;

                case 0xB000: // BNNN 	Jumps to the address NNN plus V0.
                    pc = (ushort)((opcode & 0x0FFF) + V[0]);
                    break;

                case 0xC000: // CXNN 	Sets VX to a random number and NN.
                    Random random = new Random();
                    V[(opcode & 0x0F00) >> 8] = (byte)((random.Next() % 0xFF) & (opcode & 0x00FF));
                    pc += 2;
                    break;

                case 0xD000: // DXYN 	Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels and a height of N pixels. 
                    {
                        ushort x = V[(opcode & 0x0F00) >> 8];
                        ushort y = V[(opcode & 0x00F0) >> 4];
                        ushort height = (ushort)(opcode & 0x000F);
                        ushort pixel;

                        V[0xF] = 0;
                        for (int yline = 0; yline < height; yline++)
                        {
                            pixel = Memory[I + yline];
                            for (int xline = 0; xline < 8; xline++)
                            {
                                if ((pixel & (0x80 >> xline)) != 0)
                                {
                                    if (VideoMemory[(x + xline + ((y + yline) * 64))] == 1)
                                    {
                                        V[0xF] = 1;
                                    }
                                    VideoMemory[x + xline + ((y + yline) * 64)] ^= 1;
                                }
                            }
                        }
                        NeedsScreenRefresh = true;
                        pc += 2;
                    }
                    break;
                case 0xE000:
                    switch (opcode & 0x00FF)
                    {
                        case 0x009E: // EX9E 	Skips the next instruction if the key stored in VX is pressed.
                            if (KeyState[V[(opcode & 0x0F00) >> 8]] != 0)
                                pc += 4;
                            else
                                pc += 2;
                            break;
                        case 0x0A1: // EXA1 	Skips the next instruction if the key stored in VX isn't pressed.
                            if (KeyState[V[(opcode & 0x0F00) >> 8]] == 0)
                                pc += 4;
                            else
                                pc += 2;
                            break;
                        default:
                            throw new Exception("Unknown opcode [0x0000]: " + opcode);
                    }
                    break;

                case 0xF000:
                    switch (opcode & 0x00FF)
                    {
                        case 0x0007: //FX07 	Sets VX to the value of the delay timer.
                            V[(opcode & 0x0F00) >> 8] = TimerDelay;
                            pc += 2;
                            break;

                        case 0x000A: // FX0A 	A key press is awaited, and then stored in VX.
                            {
                                bool keyPress = false;

                                for (byte i = 0; i < 16; ++i)
                                {
                                    if (KeyState[i] != 0)
                                    {
                                        V[(opcode & 0x0F00) >> 8] = i;
                                        keyPress = true;
                                    }
                                }
                                // If we didn't received a keypress, skip this cycle and try again.
                                if (!keyPress)
                                    return;

                                pc += 2;
                            }
                            break;

                        case 0x0015: // FX15 	Sets the delay timer to VX.
                            TimerDelay = V[(opcode & 0x0F00) >> 8];
                            pc += 2;
                            break;

                        case 0x0018: // FX18 	Sets the sound timer to VX.
                            TimerSound = V[(opcode & 0x0F00) >> 8];
                            pc += 2;
                            break;

                        case 0x001E: // FX1E 	Adds VX to I.
                            if (I + V[(opcode & 0x0F00) >> 8] > 0xFFF)  // VF is set to 1 when range overflow (I+VX>0xFFF), and 0 when there isn't.
                                V[0xF] = 1;
                            else
                                V[0xF] = 0;
                            I += V[(opcode & 0x0F00) >> 8];
                            pc += 2;
                            break;

                        case 0x0029: // FX29 	Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font.
                            I = (ushort)(V[(opcode & 0x0F00) >> 8] * 0x5);
                            pc += 2;
                            break;

                        case 0x0033: // FX33 	Stores the Binary-coded decimal representation of VX, with the most significant of three digits at the address in I, the middle digit at I plus 1, and the least significant digit at I plus 2. 
                            Memory[I] = (byte)(V[(opcode & 0x0F00) >> 8] / 100);
                            Memory[I + 1] = (byte)((V[(opcode & 0x0F00) >> 8] / 10) % 10);
                            Memory[I + 2] = (byte)((V[(opcode & 0x0F00) >> 8] % 100) % 10);
                            pc += 2;
                            break;

                        case 0x0055: // FX55 	Stores V0 to VX in memory starting at address I.
                            for (int i = 0; i <= (opcode & 0x0F00) >> 8; ++i)
                                Memory[I + i] = V[i];
                            I += (ushort)(((opcode & 0x0F00) >> 8) + 1);
                            pc += 2;
                            break;

                        case 0x0065: // FX65 	Fills V0 to VX with values from memory starting at address I.
                            for (int i = 0; i <= (opcode & 0x0F00) >> 8; ++i)
                                V[i] = Memory[I + i];
                            I += (ushort)(((opcode & 0x0F00) >> 8) + 1);
                            pc += 2;
                            break;

                        default:
                            throw new Exception("Unknown opcode [0x0000]: " + opcode);
                    }


                    break;

                default:
                    throw new Exception("Unknown opcode: " + opcode);
            }
            #endregion

            if (sw.ElapsedMilliseconds > LastTick2 + 6) //6
            {
                LastTick2 = sw.ElapsedMilliseconds;
                if (TimerDelay > 0)
                    TimerDelay--;

                if (TimerSound > 0)
                {
                    if (TimerSound == 1)
                        NeedsSoundPlayed = true;
                    TimerSound--;
                }

            }
        }

        private void WriteMemory(ushort location, byte value)
        {
            Memory[location] = value;
        }

        private byte ReadMemory(ushort location)
        {
            return Memory[location];
        }

    }
}
