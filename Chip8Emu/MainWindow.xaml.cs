﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Chip8Emu
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Chip8CPU = new Chip8();
            player = new System.Media.SoundPlayer("beep.wav");
        }

        Chip8 Chip8CPU;
        DispatcherTimer CycleTimer;
        System.Media.SoundPlayer player;

        private void Open_Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "c8 Files (.c8)|*.c8|All Files (*.*)|*.*";
            if (CycleTimer != null)
                CycleTimer.Stop();
            if (od.ShowDialog() == true)
            {
                Chip8CPU.Reset();
                Chip8CPU.LoadFile(od.FileName);

                //System.Threading.Thread th = new System.Threading.Thread(() =>
                //{
                //    while (true)
                //    {
                //        RunCycle(null, null);
                //        //System.Threading.Thread.Sleep(0);
                //    }
                //});
                //th.Start();
                CycleTimer = new DispatcherTimer(DispatcherPriority.Background);
                CycleTimer.Tick += new EventHandler(RunCycle);
                CycleTimer.Interval =  new TimeSpan(0, 0, 0, 0, 0);

                CycleTimer.Start();
            }
        }

        private void RunCycle(object sender, EventArgs e)
        {
           // CycleTimer.Stop();
 
            Chip8CPU.EmulateCycle();


            //if (Chip8CPU.NeedsScreenRefresh)
            //{

            //    GameScreen.Children.Clear();
            //    for (int y = 0; y < 32; ++y)
            //        for (int x = 0; x < 64; ++x)
            //        {

            //            Rectangle r = new Rectangle();
            //            r.Width = 10;
            //            r.Height = 10;
            //            if (Chip8CPU.VideoMemory[(y * 64) + x] == 0)
            //                r.Fill = Brushes.Black;
            //            else
            //                r.Fill = Brushes.White;
            //            // ... Set canvas position based on Rect object.
            //            Canvas.SetLeft(r, x * 10);
            //            Canvas.SetTop(r, y * 10);

            //            // ... Add to canvas.
            //            GameScreen.Children.Add(r);
            //        }

            //    Chip8CPU.NeedsScreenRefresh = false;
            //}

            if (Chip8CPU.NeedsScreenRefresh)
            {

                lock (GameScreen.rects)
                {
                    GameScreen.rects.Clear();
                    for (int y = 0; y < 32; ++y)
                        for (int x = 0; x < 64; ++x)
                        {

                            if (Chip8CPU.VideoMemory[(y * 64) + x] == 0)
                                GameScreen.rects.Add(new MyCanvas.MyRect() { Brush = Brushes.Black, Rect = new Rect(x * 10, y * 10, 10, 10) });
                            else
                                GameScreen.rects.Add(new MyCanvas.MyRect() { Brush = Brushes.White, Rect = new Rect(x * 10, y * 10, 10, 10) });
                        }
                }
                this.Dispatcher.Invoke((Action)(() =>
                {
                    GameScreen.InvalidateVisual();
                }));

                Chip8CPU.NeedsScreenRefresh = false;
              
                }
            if (Chip8CPU.NeedsSoundPlayed)
            {
                player.Play();
                Chip8CPU.NeedsSoundPlayed = false;
            }

            CycleTimer.Start();
        }

        private void GameScreen_KeyDown(object sender, KeyEventArgs e)
        {
				if (e.Key == Key.NumPad1)
				    Chip8CPU.KeyState [0x1] = 1;
				else if (e.Key == Key.NumPad2)
					Chip8CPU.KeyState [0x2] = 1;
				else if (e.Key == Key.NumPad3)
					Chip8CPU.KeyState [0x3] = 1;
				else if (e.Key == Key.NumPad4)		
					Chip8CPU.KeyState [0xC] = 1;
				else if (e.Key == Key.Q)		
					Chip8CPU.KeyState [0x4] = 1;
				else if (e.Key == Key.W)
					Chip8CPU.KeyState [0x5] = 1;
				else if (e.Key == Key.E)
					Chip8CPU.KeyState [0x6] = 1;
				else if (e.Key == Key.R)
					Chip8CPU.KeyState [0xD] = 1;
				else if (e.Key == Key.A)
					Chip8CPU.KeyState [0x7] = 1;
				else if (e.Key == Key.S)
					Chip8CPU.KeyState [0x8] = 1;
				else if (e.Key == Key.D)
					Chip8CPU.KeyState [0x9] = 1;
				else if (e.Key == Key.F)
					Chip8CPU.KeyState [0xE] = 1;
				else if (e.Key == Key.Z)
					Chip8CPU.KeyState [0xA] = 1;
				else if (e.Key == Key.X)
					Chip8CPU.KeyState [0x0] = 1;
				else if (e.Key == Key.C)
					Chip8CPU.KeyState [0xB] = 1;
				else if (e.Key == Key.V)
					Chip8CPU.KeyState [0xF] = 1;
        }

        private void GameScreen_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.NumPad1)
                Chip8CPU.KeyState[0x1] = 0;
            else if (e.Key == Key.NumPad2)
                Chip8CPU.KeyState[0x2] = 0;
            else if (e.Key == Key.NumPad3)
                Chip8CPU.KeyState[0x3] = 0;
            else if (e.Key == Key.NumPad4)
                Chip8CPU.KeyState[0xC] = 0;
            else if (e.Key == Key.Q)
                Chip8CPU.KeyState[0x4] = 0;
            else if (e.Key == Key.W)
                Chip8CPU.KeyState[0x5] = 0;
            else if (e.Key == Key.E)
                Chip8CPU.KeyState[0x6] = 0;
            else if (e.Key == Key.R)
                Chip8CPU.KeyState[0xD] = 0;
            else if (e.Key == Key.A)
                Chip8CPU.KeyState[0x7] = 0;
            else if (e.Key == Key.S)
                Chip8CPU.KeyState[0x8] = 0;
            else if (e.Key == Key.D)
                Chip8CPU.KeyState[0x9] = 0;
            else if (e.Key == Key.F)
                Chip8CPU.KeyState[0xE] = 0;
            else if (e.Key == Key.Z)
                Chip8CPU.KeyState[0xA] = 0;
            else if (e.Key == Key.X)
                Chip8CPU.KeyState[0x0] = 0;
            else if (e.Key == Key.C)
                Chip8CPU.KeyState[0xB] = 0;
            else if (e.Key == Key.V)
                Chip8CPU.KeyState[0xF] = 0;
        }
    }

    class MyCanvas : Canvas
    {
        public class MyRect
        {
            public Rect Rect;
            public Brush Brush;
        }

        public List<MyRect> rects = new List<MyRect>();

        protected override void OnRender(System.Windows.Media.DrawingContext dc)
        {
            base.OnRender(dc);
            lock (rects)
            {
                for (int i = 0; i < rects.Count; i++)
                {
                    MyRect mRect = rects[i];
                    dc.DrawRectangle(mRect.Brush, null, mRect.Rect);
                }
            }
        }
    }
}
